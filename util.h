#include <random>
#include <string>
#include <cmath>


class Util {
public:
	template <typename T>
	static T random(const T min, const T max);
	static std::string formatList(auto container);
	template <typename T>
	static bool isPrime(T number);
};



template <typename T>
T Util::random(const T min, const T max) {
	std::random_device device;
	std::mt19937 generator(device());
	std::uniform_int_distribution<int> distribution(min, max);
	return distribution(generator);
}

std::string Util::formatList(auto container) {
	std::string str;
	for(auto &val : container) {
		str += std::to_string(val) + ", ";
	}
	return str.substr(0, str.size()-2) + "\n";
}

template <typename T>
bool Util::isPrime(T number) {
	bool prime = true;
	for(int i = 2; i <= sqrt(number); i++) {
		if(number % i == 0) {
			prime = false;
			break;
		}
	}
	return prime;
}
