#include <functional>


template <typename T, typename CONTAINER>
class NumberHandler {
private:
	CONTAINER data;

public:
  void fill(int count, std::function< T()> generator);
  const CONTAINER& list() const;
  CONTAINER select(std::function<bool(T)> filter);
};



template <typename T, typename CONTAINER>
void NumberHandler<T, CONTAINER>::fill(int count, std::function< T()> generator) {
	for(int i = 0; i < count; i++) {
		data.push_back(generator());
	}
}

template <typename T, typename CONTAINER>
const CONTAINER& NumberHandler<T, CONTAINER>::list() const {
	return data;
}

template <typename T, typename CONTAINER>
CONTAINER NumberHandler<T, CONTAINER>::select(std::function<bool(T)> filter) {
	CONTAINER d;
	for(auto &val : data) {
		if(filter(val)) {
			d.push_back(val);
		}
	}
	return d;
}
